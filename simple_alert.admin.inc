<?php

/**
 * @file
 * Settings.
 */

/**
 * Settings form
 *
 * @return array
 *   Form array.
 */
function simple_alert_settings() {
  $form = array();
  $form['simple_alert_enable'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Alert'),
    '#default_value' => variable_get('simple_alert_enable', 1),
    '#options' => array(
      1 => t('Enabled'),
      0 => t('Disabled'),
    ),
  );
  $simple_alert_body = variable_get('simple_alert_body');
  $form['simple_alert_fieldset']['simple_alert_body'] = array(
    '#type' => 'text_format',
    '#base_type' => 'textarea',
    '#title' => t('Alert body text'),
    '#default_value' => $simple_alert_body['value'],
    '#format' => isset($simple_alert_body['format']) ? $simple_alert_body['format'] : NULL,
  );
  $form['simple_alert_fieldset']['simple_alert_type'] = array(
    '#type' => 'radios',
    '#title' => t('Type of alert'),
    '#description' => t('The type of alert that should be displayed.'),
    '#default_value' => variable_get('simple_alert_type', 1),
    '#options' => array(
      'status' => t('Status'),
      'warning' => t('Warning'),
      'error' => t('Error'),
    ),
  );
  $form['simple_alert_fieldset']['simple_alert_check_cookie'] = array(
    '#type' => 'radios',
    '#title' => t('Check cookie'),
    '#description' => t('If enabled, message will be displayed only once in every x minutes. (Set the number of minutes below.)'),
    '#default_value' => variable_get('simple_alert_check_cookie', 1),
    '#options' => array(
      1 => t('Enabled'),
      0 => t('Disabled'),
    ),
  );
  $form['simple_alert_fieldset']['simple_alert_time_cookie'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie lifetime in minutes'),
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('The number of minutes between showing the message - set to 0 for the life of the browser session.'),
    '#default_value' => variable_get('simple_alert_time_cookie', 1),
  );
  return system_settings_form($form);
}

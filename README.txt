About Simple Alert for Drupal
-----------------------------
Provides a simple way to display a drupal alert on a site, can be customised to:

1. Use a cookie to display either every n minutes, or once per browser session.
2. Use various Drupal alert types (i.e. warning, info etc).
3. Can easily be turned on and off, or have options changed on the admin page.
4. Utilises permissions for displaying and editing the alerts (ie only show to anon users.)

Admin page: admin/config/user-interface/simple_alert
